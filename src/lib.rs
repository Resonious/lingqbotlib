#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate rand;
extern crate restson;

use rand::{thread_rng, Rng, distributions};
use regex::{Regex, RegexBuilder};
use restson::{RestClient, RestPath};
use restson::Error::HttpError;



// https://www.lingq.com/apidocs/api-2.0.html#create-resource
#[derive(Serialize,Deserialize)]
pub struct Lesson {
    title: String,
    text: String,
    status: String,
    collection: i64
}
impl RestPath<()> for Lesson {
    fn get_path(_: ()) -> Result<String,restson::Error> { Ok(String::from("api/v2/ja/lessons/")) }
}


// "contentId": 2854004,
// "collectionId": 508994,
// "collectionTitle": "Quick Imports",
// "url": "https://www.lingq.com/api/v2/ja/lessons/2854004/",
// "originalUrl": null,
// "imageUrl": "https://static.lingq.com/static/images/default-content.gif",
// "title": "test one",
// "description": "",
// "duration": 0,
// "audio": null,
// "audioPending": false,
// "pubDate": "2019-09-22",
// "sharedDate": null,
// "sharedByName": "nigelbaillie",
// "giveRoseUrl": "/api/v2/ja/lessons/2854004/give_rose/",
// "wordCount": 3,
// "uniqueWordCount": 3,
// "status": "D",
// "pos": 0,
// "price": 0,
// "lessonURL": "https://www.lingq.com/en/learn/ja/web/lesson/2854004"
#[derive(Serialize,Deserialize)]
#[allow(non_snake_case)]
pub struct FullLesson {
    lessonURL: String // All we really need is this...
}


pub struct Bot {
    collection_pk: i64,
    lingq: RestClient,
}


impl Bot {
    /// Create a new Bot with the given API key and collection ID
    pub fn new(api_key: &str, collection_pk: i64) -> Bot {
        let mut bot = Bot {
            collection_pk: collection_pk,
            lingq: RestClient::new("https://www.lingq.com").expect("Failed to create LingQ REST client.")
        };
        bot.lingq.set_header("Authorization", &format!("Token {}",api_key)).expect("Failed to set header?");
        bot
    }


    /// Sends a lesson to LingQ and returns its URL (or None if it didn't work)
    pub fn create_lesson(&mut self, lesson: Lesson) -> Option<String> {
        match self.lingq.post_capture::<_,_,FullLesson>((), &lesson) {
            Ok(result) => Some(result.lessonURL),
            Err(HttpError(status, message)) => {
                println!("LingQ request returned {} {}", status, message);
                None
            }
            Err(e) => {
                println!("LingQ request failed: {}", e);
                None
            }
        }
    }


    /// Parses a message and gives you a lesson you can upload
    pub fn parse_message(&self, message: &str) -> Option<Lesson> {
        lazy_static! {
            static ref SEEMS_JAPANESE: Regex = RegexBuilder::new(r"[\u3000-\u303F]|[\u3040-\u309F]|[\u30A0-\u30FF]|[\uFF00-\uFFEF]|[\u4E00-\u9FAF]|[\u2605-\u2606]|[\u2190-\u2195]|\u203B")
                .case_insensitive(true)
                .build()
                .expect("Regex SEEMS_JAPANESE failed to compile");
        }

        // If there is any Japanese, we will consider it okay to generate a lesson for.
        if SEEMS_JAPANESE.is_match(message) {
            // Generated titles will have a 6-character "hash" in them.
            let random: String = thread_rng()
                .sample_iter(&distributions::Alphanumeric)
                .take(6)
                .collect();

            Some(Lesson {
                title:      format!("Bot Lesson {}", random),
                text:       String::from(message),
                status:     String::from("private"),
                collection: self.collection_pk
            })
        }
        else {
            // No Japanese text means no lesson!
            None
        }
    }
}
